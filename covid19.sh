#!/bin/bash
#
# edited by Semigod
#
# 2020/08/13
# 2021/05/18

updateData () {
    ## $1: output file
    ## $2: data url
    dataFile=$1
    dataUrl=$2
    curl -s -o ${dataFile} ${dataUrl}
    dos2unix -q ${dataFile}
    echo -e "  >> ${dataFile}"
}

twStatsNormal () {
    twStatsTitle=($(cat ${twStatsFile} | sed -n '1p' | sed 's/,/\ /g'))
    twStatsData=($(cat ${twStatsFile} | sed -n '2p' | \
                   sed -e 's/,\([0-9][0-9][0-9]\)"/\1"/g' -e 's/"//g' \
                       -e 's/,/\ /g'))
}

sum () {
    countFile=${1}
    num=($(cat ${countFile} | cut -d, -f3))
    count=0
    for ((i=0;i<${#num[@]};i++)); do
        count=$(expr ${count} + ${num[$i]})
    done
    echo ${count}
}

twCovid19Normal () {
    localCaseFile='localCase.data'
    importedCaseFile='importedCase.data'
    countyFile='county.data'

    ## 1      ,2      ,3      ,4  ,5  ,6           ,7    ,8
    ## 確定病名,發病年份,發病月份,縣市,性別,是否為境外移入,年齡層,確定病例數

    ## local cases
    ## 為本土案例
    cat ${twCovid19File} | sed '1d' | \
    awk -F',' '{print $4","$6","$8","$5}' | grep ',否,' | sort > ${localCaseFile}

    localCaseCount=$(sum ${localCaseFile})

    ## imported cases
    ## 為境外移入案例
    cat ${twCovid19File} | sed '1d' | \
    awk -F',' '{print $4","$6","$8","$5}' | grep ',是,' | sort > ${importedCaseFile}

    importedCaseCount=$(sum ${importedCaseFile})

    ## check county name
    impactedCounty=($(cat ${localCaseFile} | cut -d, -f1 | sort | uniq))
    impactedCountyCount=${#impactedCounty[@]}
    
    totalCountyCount=$(cat ${countyFile} | wc -l)
}

countyTotal () {
    local countyName=$1
    cat ${localCaseFile} | grep ${countyName} | awk -F',' '{sum += $3} END {print sum}'
}

function countyMale () {
    local countyName=$1
    cat ${localCaseFile} | grep ${countyName} | grep 'M' \
    | awk -F',' '{sum += $3} END {if (sum != 0) print sum; else print 0}'
}

function countyFemale () {
    local countyName=$1
    cat ${localCaseFile} | grep ${countyName} | grep 'F' \
    | awk -F',' '{sum += $3} END {if (sum != 0) print sum; else print 0}'
}

function rank () {
    impactedCountyRankFile='impactedCountyRank.data'
    for ((i=0;i<${impactedCountyCount};i++)); do
        impactedCountyCase[$i]=$(countyTotal ${impactedCounty[$i]})
        impactedCountyCaseMale[$i]=$(countyMale ${impactedCounty[$i]})
        impactedCountyCaseFemale[$i]=$(countyFemale ${impactedCounty[$i]})

        echo "${impactedCounty[$i]},${impactedCountyCase[$i]},${impactedCountyCaseMale[$i]},${impactedCountyCaseFemale[$i]}" >> ${impactedCountyRankFile}.tmp
    done

    cat ${impactedCountyRankFile}.tmp | sort -rn -t, -k2 > ${impactedCountyRankFile}
    rm -f ${impactedCountyRankFile}.tmp
}

## COVID-19 Taiwan Stats 台灣COVID-19最新統計數字
twStatsFile='covid19_tw_stats.csv'
twStatsUrl='https://od.cdc.gov.tw/eic/covid19/covid19_tw_stats.csv'

## 地區年齡性別統計表-嚴重特殊傳染性肺炎 
twCovid19File='Age_County_Gender_19Cov.csv'
twCovid19Url='https://od.cdc.gov.tw/eic/Age_County_Gender_19Cov.csv'

twCovid19JsonFile='Age_County_Gender_19Cov.json'
twCovid19JsonUrl='https://od.cdc.gov.tw/eic/Age_County_Gender_19Cov.json'

## get all data
echo -e "\n[TASK] 更新資料來源(疾病管制署資料開放平台)"
echo -e " - COVID-19 Taiwan Stats 台灣COVID-19最新統計數字"
updateData ${twStatsFile} ${twStatsUrl}
echo -e " - 地區年齡性別統計表-嚴重特殊傳染性肺炎"
updateData ${twCovid19File} ${twCovid19Url}
updateData ${twCovid19JsonFile} ${twCovid19JsonUrl}

## normal data
echo -e "\n[TASK] 資料正規化處理"
twStatsNormal
twCovid19Normal

## rank for local case in the county
rank

## display

echo -e "\n[RESULT]"
echo -e "\n# 台灣COVID-19最新統計數字"
printf "  %-8s %-14s %-8s %-10s %-10s %-14s %-14s %-14s\n" "${twStatsTitle[0]}" \
                                                   "${twStatsTitle[1]}" \
                                                   "${twStatsTitle[2]}" \
                                                   "${twStatsTitle[3]}" \
                                                   "${twStatsTitle[4]}" \
                                                   "${twStatsTitle[5]}" \
                                                   "${twStatsTitle[6]}" \
                                                   "${twStatsTitle[7]}"
printf "  %-6s %-10s %-6s %-8s %-8s %-10s %-10s %-10s\n" "${twStatsData[0]}" \
                                                   "${twStatsData[1]}" \
                                                   "${twStatsData[2]}" \
                                                   "${twStatsData[3]}" \
                                                   "${twStatsData[4]}" \
                                                   "${twStatsData[5]}" \
                                                   "${twStatsData[6]}" \
                                                   "${twStatsData[7]}"

echo -e "\n# 感染人數比例為"
echo -e "  本土 ：境外移入 = ${localCaseCount} ： ${importedCaseCount}"

echo -e "\n# 目前有本土感染的城市有 ${impactedCountyCount} / ${totalCountyCount} 個，分別是\n"
printf "  %-10s\t%-6s\t%-10s\n" "城市" "確診人數" "男/女"
#cat ${impactedCountyRankFile} | sed -e 's/^/\ \ /' \
#                                    -e 's/,/\t/' \
#                                    -e 's/,/\t(男:/' \
#                                    -e 's/,/\t女:/' \
#                                    -e 's/$/)/'
cat ${impactedCountyRankFile} | sed -e 's/^/\ \ /' \
                                    -e 's/,/\t/' \
                                    -e 's/,/\t\t/' \
                                    -e 's/,/\//'
#                                    -e 's/$/)/'

#cat ${impactedCountyRankFile} | awk -F',' '{boy+=$3;girl+=$4} END {print boy","girl}' \
# | sed -e 's/^/\t\t\t    /' -e 's/,/\t   /'

cat ${impactedCountyRankFile} | awk -F',' '{boy+=$3;girl+=$4} END {print boy","girl}' \
 | sed -e 's/^/\t\t\t\t/' -e 's/,/\//'

echo