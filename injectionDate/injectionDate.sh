#!/bin/bash
#
# edited by semigod
#
# 2022/01/15

period="+12 weeks"
checkDate=${1:-2021-10-01}

date -d "${checkDate} ${period}" +%Y-%m-%d