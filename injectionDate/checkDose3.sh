#!/bin/bash
#
# edited by semigod
#
# 2022/01/16

function checkInjection () {
    local dose2Date=$1
    date -d "${dose2Date} ${injectionPeriod}" +%Y-%m-%d
}

function dateFile () {
    for i in $(seq 0 ${period}); do 
        dose2DateList[$i]=$(date -d "${startDate} +${i} days" +%Y-%m-%d)
        dose3DateList[$i]=$(checkInjection "${dose2DateList[$i]}")
        #echo "${dose2DateList[$i]},${dose3DateList[$i]}"
    done
}

function dose3DateFile () {
    dateFile="dose3Date"
    echo "第二劑日期,第三劑日期(12週)" > ${dateFile}.csv
    count=${#dose3DateList[@]}
    for ((i=0;i<${count};i++)); do
        echo "${dose2DateList[$i]},${dose3DateList[$i]}" >> ${dateFile}.csv
    done
}

startDate="2021-10-01"
period=180

injectionPeriod="+12 weeks"

dateFile
##
dose3DateFile
## convert csv to xlsx
ssconvert ${dateFile}.csv ${dateFile}.xlsx