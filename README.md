# 資料來源取得說明

## 查詢資料來源

- [疾病管制署資料開放平台](https://data.cdc.gov.tw/)

 > [COVID-19台灣最新病例、檢驗統計](https://data.cdc.gov.tw/dataset/covid19_tw__stats)

 > [地區年齡性別統計表-嚴重特殊傳染性肺炎](https://data.cdc.gov.tw/dataset/agstable-19cov)

- [傳染病統計資料查詢系統](https://nidss.cdc.gov.tw/nndss/DiseaseMap?id=19CoV)

 > 本次查詢的日期範圍為2020/01/01至2020/12/31。

 > 本查詢結果為系統自動產生，數據隨時可能因未來修正而變動。

 > 目前嚴重特殊傳染性肺炎境外移入病例多於機場或集中檢疫所採檢確診並即隔離治療，故不顯示地理分佈資訊。

## 程式執行截圖

![reult.png](./pics/001.png)

---
## 免責聲明
相關數據請依照**疾病管制署**為主。